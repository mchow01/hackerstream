var socket = io();

function renderTweet(tweet) {
	$("#default").hide();
	text = tweet.text;
	source = tweet.source;
	created_at =tweet.created_at;
	date = new Date(Date.parse(created_at));
	profile_image = tweet.user.profile_image_url_https;
	screen_name = tweet.user.screen_name;
	$("#stream").prepend('<div class="message"><p class="content">' + text + '</p><p><img src="'+ profile_image + '" alt="profile image" /> <span class="username">@' + screen_name + '</span>, <span class="timestamp">' + date + '</span> via ' + source + '</p></div>');
}

$(document).ready(function() {
	if (localStorage.length > 0) {
		for (var timestamp in localStorage) {
			if (timestamp !== "debug") {
				renderTweet(JSON.parse(localStorage[timestamp]));
			}
		}
	}
});

socket.on('tweet', function(tweet) {
	try {
		localStorage[new Date().getTime()] = JSON.stringify(tweet);
		renderTweet(tweet);
	}
	catch (e) {
		if (e == QUOTA_EXCEEDED_ERR) {
			localStorage.clear();
		}
	}
});
