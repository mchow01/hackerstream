var express = require('express');
var app = express();
var compress = require('compression'); // Gzip compression
var logger = require('morgan'); // Logging
app.use(compress());
app.use(logger('dev'));
app.use(express.static(__dirname + '/public'));

var http = require('http').Server(app);
var io = require('socket.io')(http);
var escape = require('escape-html');

var Twitter = require('node-tweet-stream');
var t = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    token: process.env.TWITTER_TOKEN,
    token_secret: process.env.TWITTER_TOKEN_SECRET
  });
 
t.on('tweet', function (tweet) {
	io.emit('tweet', tweet);
});
 
t.on('error', function (err) {
	io.emit("error", err);
});
 
t.track('defcon');
t.track('bsideslv');
t.track('blackhat');
t.track('RSAC');
t.track('RSAC2016');
t.track('RSAConference');

app.get('/', function(request, response) {
	response.sendFile(__dirname + '/index.html');
});

http.listen(process.env.PORT || 3000, function() {
	console.log('Server is on...');
});